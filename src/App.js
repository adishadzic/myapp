import "./App.css";
// import ExtendedClock from "./components/ExtendedClock";
import DateAndTime from "./components/DateAndTime";
import List from "./components/List";
import SomeForm from "./components/SomeForm";
// import Context from "./components/Context";

function App() {
  return (
    <div className="App">
      {/* <ExtendedClock /> */}
      {/* <Toggle /> */}
      <DateAndTime />
      <List />
      <SomeForm />
      {/* <Context /> */}
    </div>
  );
}

export default App;
