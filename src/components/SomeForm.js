import React, { useState } from "react";

function SomeForm() {
  const [userData, setUserData] = useState({ firstName: "", lastName: "" });

  function handleChange(event) {
    setUserData({ ...userData, [event.target.name]: event.target.value });
    // console.log(event.target.name, "changed to", event.target.value);
    console.log(userData);
  }

  return (
    <div>
      <div>Welcome, {userData.firstName + " " + userData.lastName}</div>
      <div>
        <label>First name:</label>
        <input
          value={userData.firstName}
          name="firstName"
          onChange={handleChange}
        />
      </div>
      <div>
        <label>Last name:</label>
        <input
          value={userData.lastName}
          name="lastName"
          onChange={handleChange}
        />
      </div>
    </div>
  );
}

export default SomeForm;
