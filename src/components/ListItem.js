import React from "react";
import "../App.css";

function ListItem(props) {
  const users = props.users;
  const listItems = users.map((user) => (
    <tr key={user.id}>
      <td>{user.username}</td>
      <td>{user.age}</td>
      <td>{user.registrationDate}</td>
    </tr>
  ));
  return (
    <div className="table">
      <table id="users_table">
        <thead>
          <tr>
            <th>Username</th>
            <th>Age</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>{listItems}</tbody>
      </table>
    </div>
  );
}

export default ListItem;
