import React from "react";
import ListItem from "./ListItem";
import "../App.css";

export default class List extends React.Component {
  state = {
    users: [
      { username: "jim", age: 32, registrationDate: "08 / 04 / 2021", id: 1 },
      {
        username: "michael",
        age: 45,
        registrationDate: "08 / 03 / 2021",
        id: 2,
      },
      { username: "pam", age: 36, registrationDate: "08 / 02 / 2021", id: 3 },
      {
        username: "dwight",
        age: 31,
        registrationDate: "05 / 04 / 2021",
        id: 4,
      },
      {
        username: "angela",
        age: 42,
        registrationDate: "06 / 04 / 2021",
        id: 5,
      },
    ],
    count: 0,
  };

  componentDidMount() {
    this.setState((state) => ({ count: state.users.length }));
  }

  render() {
    return (
      <div className="list_item">
        <ListItem users={this.state.users} />

        <div>
          <h2>Number of users: {this.state.count}</h2>
        </div>
      </div>
    );
  }
}
