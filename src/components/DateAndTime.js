import React from "react";

export default class Clock extends React.Component {
  state = { date: new Date() };

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date(),
    });
  }

  render() {
    return (
      <div>
        <div>{this.state.date.toLocaleString()}</div>
      </div>
    );
  }
}

// import React, { useState, useEffect } from "react";

// function Clock() {
//   let [count, setCount] = useState(0);

//   useEffect(() => {
//     let counter = setInterval(() => {
//       setCount((count) => count + 1);
//     }, 5000);

//     return () => {
//       clearInterval(counter);
//     };
//   }, []);

//   return <div>{count}</div>;
// }

// export default Clock;
